---
format: 
  revealjs:
    theme: [default, style.scss]
    width: 1600
    height: 900
    footer: 'Elio Lagunes Díaz. Inecol, 2023'
    chalkboard: 
      buttons: false
---

#  {.title}

::: r-fit-text

[Principios cuantitativos]{.flow}

[de minería de texto]{.flow}
:::

::: footer
[  ](google.com)
:::

## Requisitos {chalkboard-buttons="true"}

Saber:

* qué es sujeto (tácito o explícito) y predicado
* qué es lexema y morfema
* qué es adjetivo, adverbio, conjunción, determinante, preposiciones (< 5 s), sujeto, verbo
* el pretérito imperfecto y el antepresente de programar


#  {.title}

::: r-fit-text

[Un tópico es un conjunto de palabras y]{.flow}
---
[un documento es un conjunto de tópicos]{.flow}

:::

::: footer
[  ](google.com)
:::


#  {.title}

::: r-fit-text
[Tipos de minería de texto: diccionarios]{.flow}
:::

::: footer
[  ](google.com)
:::



## Tipos de minería de texto: Diccionarios

> Los métodos de diccionario identifican automáticamente palabras de un listado en los textos y, por esta razón, son particularmente útiles en cuantificar la presencia de un concepto de interés en grandes corpora. - Ana Makanovic, 2022

### Ejemplos:

Palabras que denoten un afecto negativo o positivo: análisis de sentimiento.

```{r}
#| echo: false

library(tidytext)
library(ggplot2)

get_sentiments("afinn") |> as.data.frame() |> head()
  
 
```


## Tipos de minería de texto: Diccionarios (continúa)

Distribución del sentimiento en las palabras de un diccionario (Informatics and Mathematical Modelling, Technical University of Denmark, 2011) para análisis del sentimiento
(obsérvese que solo son 2477 para todo el idioma ingles, porque no incluye las neutrales)

```{r}
get_sentiments("afinn") |> ggplot(aes(value)) + geom_histogram() +
  labs(title = "palabras diccionario AFINN-III")

```

## Tipos de minería de texto: Diccionarios (continúa)

Como solo buscan palabras, no requieren mucho poder de cómputo y pueden analizar millones de documentos.

*En un [artículo revisan 509 millones de tweets](https://www.sciencedirect.com/science/article/pii/S0049089X22000904#bib43), donde encuentran que los tweets tienden a ser más positivos en fines de semana y mañanas, aportando a la hipótesis de que las redes reflejan los ritmos biológicos.*

Una limitación importante es que son específicos de dominios, una palabra como "impuesto" puede tener un significado negativo en sociología y uno neutro en estudios financieros.

#  {.title}

::: r-fit-text
[Tipos de minería de texto: Análisis semántico y de redes]{.flow}
:::

::: footer
[  ](google.com)
:::


## Tipos de minería de texto: análisis semántico y de redes

Algoritmos que identifican las tripletas Sujeto-Acción-Objeto asignando roles semánticos a las palabras en una oración.

Mediante el [análisis de 130,000 notas periodísticas](https://www.sciencedirect.com/science/article/pii/S0049089X22000904#bib111) de la elección de 2012 en EE.UU, se muestra que los políticos republicanos hablan más negativamente de los demócratas que viceversa.

![Esquema de análisis semántico, Elio Lagunes, 2023](toledo.png)

## Tipos de minería de texto: análisis semántico y de redes (continúa)

Reconocimiento de entidades nominadas (NER). Son algoritmos de inteligencia artificial que, a partir de un entrenamiento, identifican lugares, tiempos y personas en el texto, no como un diccionario, sino por las funciones que tienen:

* Washington empató con Kansas
* En Washington se celebrará la cumbre
* Washington viajó a México para filmar su película


#  {.title}

::: r-fit-text
[Tipos de minería de texto: Agrupamientos (clustering) no supervisados ]{.flow}
:::

## Tipos de minería de texto: Agrupamientos (clustering) no supervisados

* Buscan identificar las maneras amplias en cómo se abordan los conceptos en los corpora.
* Se conocen como técnicas de _modelado de tópicos_
* La más popular: Latent Dirichlet Allocation

En un [artículo en que usaron 8737 abstracts](https://www.sciencedirect.com/science/article/pii/S0049089X22000904#bib103) de publicaciones de sociología a lo largo del eje cualitativo-cuantitativo, encontraron que la metodología está muy integrada al área de investigación, sin embargo existe un aumento marginal en metodologías cuantitativas en el campo en general.


#  {.title}

::: r-fit-text
[Conceptos del procesamiento]{.flow}
:::

## Destrucción del discurso

Partimos de un corpus de documentos ordenados (descargados programáticamente por lo general), limpiados, con los textos en una sola columna, en otra columna los títulos, autores y fechas, en un mismo idioma (o con idioma indicado en otra columna)

> A menudo se dice que 80% del análisis de datos transcurre en la limpieza y preparación de los datos. 
> - Hadley Wickham

```{r}
#| echo: false
library(stringr)
library(dplyr)
library(magrittr)

read.csv("corpus_sya.csv") |> mutate(texto = substr(texto, 8, 70)) |>  filter(grepl("social|socio", texto)) |>  slice(1:4) |>
magrittr::use_series(texto) |> paste0("...") |> str_view( "social|socio")
```


## Datos textuales {.textcenter}


```{r}
cuerpo = read.csv("corpus_sya.csv") |> dplyr::rename(text = texto) |> dplyr::as_tibble() |>
  mutate(text = gsub("Resumen ", "", text))
cuerpo$text[1:16] |>
  stringr::str_remove(".*\n\\w* \\w* \\w* ") |> stringr::str_sub(1,60) |> paste0("...")
```

## Datos listos para modelación {chalkboard-buttons="true"}

Ya limpiados, tokenizados, lematizados, sin stopwords, dispuestos en una matriz palabra-documento.

***¿qué significa todo eso?***

```{r}
library(recipes)
library(textrecipes)

recipe(~ text, data = cuerpo[1:18, ]) %>%
  step_tokenize(text) %>%
  step_tokenfilter(text) %>%
  step_tf(text) %>%
  prep() %>%
  bake(new_data = NULL) %>%
  as.matrix() %>%
  unname() %>%
  .[1:10, 1:11]
```

##  {background-image="https://images.unsplash.com/photo-1495640388908-05fa85288e61?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80" background-size="contain" background-position="left"}

::: columns
::: {.column width="40%"}
:::

::: {.column width="60%"}
### Flujo de trabajo: {.r-fit-text}

::: incremental
::: r-fit-text
-   Convertir el [texto]{.red} en [tokens]{.orange}
-   Modificar los [tokens]{.orange}
-   Contar (elegantemente) los [tokens]{.orange}
:::
:::
:::
:::

::: footer
Photo by <a href="https://unsplash.com/@mrrrk_smith?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">John-Mark Smith</a> on <a href="https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
:::

#  {.title}

::: r-fit-text
[Tokenización]{.flow}
:::

::: footer
[  ](google.com)
:::

## Tokenización

> Los tokens son las moléculas del discurso: las unidades mínimas que aportan significado; las letras y la puntuación corresponden al nivel de los elementos: son la sustancia sin forma con la que se construye todo, pero solo al conformarse en palabras hablarán de algo. Así como hay moléculas de un solo átomo, hay tokens de una sola letra. 
-- <cite>E. Lagunes, 2022.</cite>


```{r}
library(recipes)
library(textrecipes)

recipe(~ text, data = cuerpo[1:18, ]) %>%
  step_tokenize(text) %>%
  show_tokens(text, n = 10) %>%
  purrr::walk(~cat(substr(paste0("\"", .x, "\"", collapse = " "), 1, 60), "...\n"))
```


#  {.title}

::: r-fit-text
[Modificaciones]{.flow}
:::

::: footer
[  ](google.com)
:::

## Modificación de tokens - stemming

### Stemming vs lematización:

> Stemming es un proceso heurístico crudo que rebana los finales de las palabras, en la esperanza de lograr su propósito correctamente la mayor parte del tiempo; a menudo incluye la remoción de afijos derivacionales. La lematización usualmente se refiere a hacer las cosas propiamente con el uso de un vocabulario y un análisis morfológico de las palabras, normalmente buscando remover solamente las inflexiones y retener la base o la forma de diccionario de una palabra. 
-- <cite>User Miku, stackoverflow, 2009.</cite>


## Stemming (continúa)

<br>

```{r}
library(recipes)
library(textrecipes)

recipe(~ text, data = cuerpo) %>%
  step_tokenize(text) %>%
  step_stem(text) %>%
  show_tokens(text, n = 8) %>%
  purrr::walk(~cat(substr(paste0("\"", .x, "\"", collapse = " "), 1, 60), "...\n"))
```

## Modificación de tokens - lematización.

> "Este amoroso tormento  que en mi corazón se ve,  sé que lo siento y no sé  la causa porque lo siento.  Siento una grave agonía  por lograr un devaneo,  que empieza como deseo  y para en melancolía.  Y cuando con más terneza  mi infeliz estado lloro  sé que estoy triste e ignoro  la causa de mi tristeza." 
-- <cite>Sor Juana Inés de la Cruz </cite>

```{python}
['este', 'amoroso', 'tormento',  'que', 'en', 'mi', 'corazón', 'él', 'ver', ',',  'saber', 'que', 'él', 'sentir', 'y', 'no', 'saber',  'el', 'causa', 'porque', 'él', 'sentir', '.',  'sentir', 'uno', 'grave', 'agonía',  'por', 'lograr', 'uno', 'devaneo', ',',  'que', 'empezar', 'como', 'deseo',  'y', 'para', 'en', 'melancolía', '.',  'y', 'cuando', 'con', 'más', 'terneza',  'mi', 'infeliz', 'estado', 'lloro',  'saber', 'que', 'estar', 'triste', 'e', 'ignoro',  'el', 'causa', 'de', 'mi', 'tristeza', '.']
```

## Modificación de tokens - stopwords

```{r}
stopwords::stopwords("es")
```

## stopwords (continúa)


```{r}
library(recipes)
library(textrecipes)

recipe(~ text, data = cuerpo) %>%
  step_tokenize(text) %>%
  step_stopwords(text) %>%
  show_tokens(text, n = 8) %>%
  purrr::walk(~cat(substr(paste0("\"", .x, "\"", collapse = " "), 1, 60), "...\n"))
```


## Modificación de tokens - n-grams

<br>

```{r}
library(recipes)
library(textrecipes)

recipe(~ text, data = cuerpo) %>%
 step_tokenize(text) %>%
  step_stem(text) %>%
  step_ngram(text) %>%
  show_tokens(text, n = 7) %>%
  purrr::walk(~cat(substr(paste0("\"", .x, "\"", collapse = " "), 1, 60), "...\n"))
```


#  {.title}

::: r-fit-text
[Los números: frecuencias]{.flow}
:::


## Sacando números - Frecuencia de términos


```{r}
library(recipes)
library(textrecipes)

recipe(~ text, data = cuerpo) %>%
  step_tokenize(text) %>%
  step_stem(text) %>%
  step_ngram(text) %>%
  step_tokenfilter(text, max_tokens = 100) %>%
  step_tf(text) %>%
  prep() %>%
  bake(new_data = NULL) %>%
  as.matrix() %>%
  unname() %>%
  .[1:4, 1:11]
```

## Sacando números: frecuencia de términos - inverso de la frecuencia en documentos (tf-idf)



```{r}
library(recipes)
library(textrecipes)

recipe(~ text, data = cuerpo) %>%
  step_tokenize(text) %>%
  step_stem(text) %>%
  step_ngram(text) %>%
  step_tokenfilter(text, max_tokens = 100) %>%
  step_tfidf(text) %>%
  prep() %>%
  bake(new_data = NULL) %>%
  as.matrix() %>%
  unname() %>%
  round(4) %>%
  .[1:4, 1:8]
```

#  {.title}

::: r-fit-text
[Con qué se hace]{.flow}
:::

## Paquetes programáticos

### lenguaje R

[CRAN task view Natural Language Processing](https://cran.r-project.org/web/views/NaturalLanguageProcessing.html)

`tm, openNLP, tidytext, topicmodels, textrecipes, qdap, udpipe`

### **python** 

`NLTK, spaCy`

### Software de interfaz gráfica

[Cortex](https://managerv2.cortext.net/) de la universidad Gustave Eiffel


##  {background-image="https://images.unsplash.com/photo-1550399105-05c4a7641b02?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80" background-size="contain" background-position="right"}

::: columns
::: {.column width="40%"}

::: r-fit-text

Gracias!!!

- [Repositorio](https://gitlab.com/datamarindo)

Dos ejemplos:

- [Entrada Víctor Toledo](http://datamarindo.baselinux.net/blog/posts/11-toledo-text-mining/)

- [Entrada Madera y Bosques](http://datamarindo.baselinux.net/blog/posts/12-topic_model_MYB/)

:::

:::

::: {.column width="60%"}
:::
:::

::: footer
Photo by <a href="https://unsplash.com/@eddrobertson?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Ed Robertson</a> on <a href="https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
:::
